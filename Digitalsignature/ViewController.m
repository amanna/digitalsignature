//
//  ViewController.m
//  Digitalsignature
//
//  Created by SKETCH_IOS_01 on 29/02/16.
//  Copyright © 2016 SKETCH_IOS_01. All rights reserved.
//

#import "ViewController.h"
#import "SignatureViewQuartzQuadratic.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet SignatureViewQuartzQuadratic *viewImg;

- (IBAction)btnSaveAction:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
 [super viewDidLoad];
    NSString *searchFilename = @"abc.png"; // name of the PDF you are searching for
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDirectoryEnumerator *direnum = [[NSFileManager defaultManager] enumeratorAtPath:documentsDirectory];
    
    NSString *documentsSubpath;
    while (documentsSubpath = [direnum nextObject])
    {
        if (![documentsSubpath.lastPathComponent isEqual:searchFilename]) {
            continue;
        }
        
        NSLog(@"found %@", documentsSubpath);
    }
   
 // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSaveAction:(UIButton *)sender {
    UIGraphicsBeginImageContext(self.viewImg.frame.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, self.viewImg.frame);
    
    [self.viewImg.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *vwImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data=UIImagePNGRepresentation(vwImage);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imgName = [NSString stringWithFormat:@"%@",@"abc.png"];
    NSString *strPath = [documentsDirectory stringByAppendingPathComponent:imgName];
    [data writeToFile:strPath atomically:YES];
}
@end
