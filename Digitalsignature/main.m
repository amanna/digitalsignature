//
//  main.m
//  Digitalsignature
//
//  Created by SKETCH_IOS_01 on 29/02/16.
//  Copyright © 2016 SKETCH_IOS_01. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
